package shop.beautyservice.beautyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


//@EntityScan(basePackages= {"shop.beautyservice.beautyservice.domain"})
//@EnableJpaRepositories("shop.beautyservice.beautyservice.domain.Usuaris")
//@ComponentScan(basePackages = "shop.beautyservice.beautyservice.service.UsuariService")
@SpringBootApplication
public class BeautyserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeautyserviceApplication.class, args);
	}

}
