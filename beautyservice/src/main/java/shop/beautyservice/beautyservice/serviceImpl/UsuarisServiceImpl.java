package shop.beautyservice.beautyservice.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shop.beautyservice.beautyservice.domain.Usuaris;
import shop.beautyservice.beautyservice.repository.UsuarisRepository;
import shop.beautyservice.beautyservice.service.UsuariService;

@Service
public class UsuarisServiceImpl implements UsuariService {

   @Autowired
    private UsuarisRepository usuarisRepository;

    @Override
    public List<Usuaris> llistarTotsElsUsuaris() {
        return (List<Usuaris>)usuarisRepository.findAll();
    }

    @Override
    public Optional llistarUsuarisPerId(int id) {
        return usuarisRepository.findById(id);
    }

    @Override
    public int afexir(Usuaris u) {
        int resposta=0;
        Usuaris usuaris=usuarisRepository.save(u);
        if(!usuaris.equals(null)){
            resposta=1;
        }
        return resposta;
    }

    @Override
    public void esborrar(int id) {
        usuarisRepository.deleteById(id);
        
    }
    
}
