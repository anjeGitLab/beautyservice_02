package shop.beautyservice.beautyservice.domain;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="TipusServeis")
//public class TipusServeis implements Serializable {
  //  private static final long serialVersionUID = 1L;
    public class TipusServeis{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTipusServei;

    @Column(name = "nomTipusServei")
    @Size(max=45)
    private String nomTipusServei;    
    
    @Column(name = "descripcioTipusServei")  
    @Size(max=120)
    private String descripcioTipusServei;

  public TipusServeis(String nomTipusServei, String descripcioTipusServei ){
     this.nomTipusServei=nomTipusServei;
     this.descripcioTipusServei=descripcioTipusServei;
  }

  public TipusServeis(){}

  //@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
//private Set<Serveis> Serveis;


    /**
     * @return int return the idTipusServei
     */
    public int getIdTipusServei() {
        return idTipusServei;
    }

    /**
     * @param idTipusServei the idTipusServei to set
     */
    public void setIdTipusServei(int idTipusServei) {
        this.idTipusServei = idTipusServei;
    }

    /**
     * @return String return the nomTipusServei
     */
    public String getNomTipusServei() {
        return nomTipusServei;
    }

    /**
     * @param nomTipusServei the nomTipusServei to set
     */
    public void setNomTipusServei(String nomTipusServei) {
        this.nomTipusServei = nomTipusServei;
    }

    /**
     * @return String return the descripcioTipusServei
     */
    public String getDescripcioTipusServei() {
        return descripcioTipusServei;
    }

    /**
     * @param descripcioTipusServei the descripcioTipusServei to set
     */
    public void setDescripcioTipusServei(String descripcioTipusServei) {
        this.descripcioTipusServei = descripcioTipusServei;
    }

}
