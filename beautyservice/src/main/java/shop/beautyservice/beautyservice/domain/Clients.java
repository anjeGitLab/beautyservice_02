package shop.beautyservice.beautyservice.domain;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.Entity;

@Entity
//public class Clients Serializable {
    //  private static final long serialVersionUID = 1L;
    public class Clients {    

    @Id
    @Column(name = "idClient")
    @Size(max=9)
    private int idClient;

    public  Clients (){
    }

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	@JoinColumn(name = "usuaris_id", nullable = false)
    private Usuaris usuaris;

    /**
     * @return int return the idClient
     */
    public int getIdClient() {
        return idClient;
    }

    /**
     * @param idClient the idClient to set
     */
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }


    /**
     * @return Usuaris return the usuaris
     */
    public Usuaris getUsuaris() {
        return usuaris;
    }

    /**
     * @param usuaris the usuaris to set
     */
    public void setUsuaris(Usuaris usuaris) {
        this.usuaris = usuaris;
    }

}