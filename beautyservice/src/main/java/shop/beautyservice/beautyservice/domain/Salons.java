package shop.beautyservice.beautyservice.domain;

import javax.persistence.*;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Salons")
//public class Salons implements Serializable {
  //  private static final long serialVersionUID = 1L;
    public class Salons {    

    @Id
    @Column(name = "idSalo")
    private String idSalo;
   

    public Salons(){}

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name="id_usuaris")
    private Usuaris usuaris;

    /**
     * @return int return the idSalo
     */
    public String getIdSalo() {
        return idSalo;
    }

    /**
     * @param idSalo the idSalo to set
     */
    public void setIdSalo(String idSalo) {
        this.idSalo = idSalo;
    }


    /**
     * @return Usuaris return the usuaris
     */
    public Usuaris getUsuaris() {
        return usuaris;
    }

    /**
     * @param usuaris the usuaris to set
     */
    public void setUsuaris(Usuaris usuaris) {
        this.usuaris = usuaris;
    }

}