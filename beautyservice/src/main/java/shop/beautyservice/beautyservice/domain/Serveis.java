package shop.beautyservice.beautyservice.domain;


import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Serveis")
//public class Serveis implements Serializable {
  //  private static final long serialVersionUID = 1L;

    public class Serveis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idServei;

    public Serveis (){}
  
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name="id_ofertes")
    private Ofertes ofertes;
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name="id_serveis")
    private TipusServeis tipusServeis;


    /**
     * @return int return the idServei
     */
    public int getIdServei() {
        return idServei;
    }

    /**
     * @param idServei the idServei to set
     */
    public void setIdServei(int idServei) {
        this.idServei = idServei;
    }

    /**
     * @return Ofertes return the ofertes
     */
    public Ofertes getOfertes() {
        return ofertes;
    }

    /**
     * @param ofertes the ofertes to set
     */
    public void setOfertes(Ofertes ofertes) {
        this.ofertes = ofertes;
    }

    /**
     * @return TipusServeis return the tipusServeis
     */
    public TipusServeis getTipusServeis() {
        return tipusServeis;
    }

    /**
     * @param tipusServeis the tipusServeis to set
     */
    public void setTipusServeis(TipusServeis tipusServeis) {
        this.tipusServeis = tipusServeis;
    }

}
