package shop.beautyservice.beautyservice.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="Ofertes")
//public class Ofertes implements Serializable {
  //  private static final long serialVersionUID = 1L;
 public class Ofertes{ 

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idOferta;

    
    @NotNull
    @Column(name = "descripcio")
    @Size(max=120)
    private String descripcio;

    @NotNull
    @DateTimeFormat(pattern = "dd-MM-yyyy") 
    @Column(name = "data")
    @Size(max=45)
    private Date data; 
   
    @NotNull
    @Column(name = "intervalHorari")
    @Size(max=45)
    private String intervalHorari;     
    
    @NotNull
    @Column(name = "preu")
    private double preu;   

    public Ofertes( String descripcio, Date data, String intervalHorari, double preu){

        this.descripcio=descripcio;
        this.data=data;
        this.intervalHorari=intervalHorari;
        this.preu=preu;
    }

    public Ofertes(){}

    //@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    //private Set<Reserves> reserves;

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name="id_salons")
    private Salons salons;

    /**
     * @return int return the idOferta
     */
    public int getIdOferta() {
        return idOferta;
    }

    /**
     * @param idOferta the idOferta to set
     */
    public void setIdOferta(int idOferta) {
        this.idOferta = idOferta;
    }

    /**
     * @return String return the descripcio
     */
    public String getDescripcio() {
        return descripcio;
    }

    /**
     * @param descripcio the descripcio to set
     */
    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    /**
     * @return Date return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @return String return the intervalHorari
     */
    public String getIntervalHorari() {
        return intervalHorari;
    }

    /**
     * @param intervalHorari the intervalHorari to set
     */
    public void setIntervalHorari(String intervalHorari) {
        this.intervalHorari = intervalHorari;
    }

    /**
     * @return double return the preu
     */
    public double getPreu() {
        return preu;
    }

    /**
     * @param preu the preu to set
     */
    public void setPreu(double preu) {
        this.preu = preu;
    }


    /**
     * @return Salons return the salons
     */
    public Salons getSalons() {
        return salons;
    }

    /**
     * @param salons the salons to set
     */
    public void setSalons(Salons salons) {
        this.salons = salons;
    }

}
