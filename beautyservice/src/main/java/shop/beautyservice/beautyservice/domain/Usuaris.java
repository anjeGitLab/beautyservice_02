package shop.beautyservice.beautyservice.domain;


import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Usuaris")
//public class Usuaris implements Serializable {
  //  private static final long serialVersionUID = 1L;
public class Usuaris{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idUsuari;

    @NotNull
    @Column(name = "nom")
    @Size(max=45)
    private String nom;
    
    @NotNull
    @Column(name = "cognom")
    @Size(max=45)
    private String cognom;
    
    @NotNull
    @Column(name = "telefon")
    @Size(max=9)
    private String telefon; 
    
    @NotNull
    @Column(name = "mail")
    @Size(max=45)
    private String mail; 
    
    @NotNull
    @Column(name = "contrasenya")
    @Size(max=45)
    private String contrasenya;  
    
    @NotNull
    @Column(name = "direccio")
    @Size(max=45)
    private String direccio; 
    
    @NotNull
    @Column(name = "ciutat")
    @Size(max=45)
    private String ciutat;
    
    @NotNull
    @Column(name = "codiPostal")
    @Size(max=45)
    private String codiPostal;    
        
    public Usuaris (String nom, String cognom, String telefon, String mail, String contrasenya, String direccio, String ciutat, String codiPostal){
        this.nom =nom;
        this.cognom=cognom;
        this.telefon=telefon;
        this.mail=mail;
        this.contrasenya =contrasenya;
        this.direccio=direccio;    
        this.ciutat=ciutat;
        this.codiPostal=codiPostal;
    }

    public Usuaris (){}
/*
@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
private Set<Clients> clients;

@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
private Set<Salons> salons;    
*/
    /**
     * @return int return the idUsuari
     */
    public int getIdUsuari() {
        return idUsuari;
    }

    /**
     * @param idUsuari the idUsuari to set
     */
    public void setIdUsuari(int idUsuari) {
        this.idUsuari = idUsuari;
    }

    /**
     * @return String return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return String return the cognom
     */
    public String getCognom() {
        return cognom;
    }

    /**
     * @param cognom the cognom to set
     */
    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    /**
     * @return String return the telefon
     */
    public String getTelefon() {
        return telefon;
    }

    /**
     * @param telefon the telefon to set
     */
    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    /**
     * @return String return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return String return the contrasenya
     */
    public String getContrasenya() {
        return contrasenya;
    }

    /**
     * @param contrasenya the contrasenya to set
     */
    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }

    /**
     * @return String return the direccio
     */
    public String getDireccio() {
        return direccio;
    }

    /**
     * @param direccio the direccio to set
     */
    public void setDireccio(String direccio) {
        this.direccio = direccio;
    }

    /**
     * @return String return the ciutat
     */
    public String getCiutat() {
        return ciutat;
    }

    /**
     * @param ciutat the ciutat to set
     */
    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    /**
     * @return String return the codiPostal
     */
    public String getCodiPostal() {
        return codiPostal;
    }

    /**
     * @param codiPostal the codiPostal to set
     */
    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }


}
