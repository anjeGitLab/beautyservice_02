package shop.beautyservice.beautyservice.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;

@Entity
@Table(name="Reserves")
//public class Reserves implements Serializable {
  //  private static final long serialVersionUID = 1L; 
 public class Reserves {   

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idReserva;


    @Column(name="estat")
    private int estat;  
    
    public Reserves(int estat){
        this.estat=estat;
    }

    public Reserves(){}
    
    @ManyToOne
    @JoinColumn(name="id_clients")
    private Clients clients;
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name="id_ofertes")
    private Ofertes ofertes;

    /**
     * @return int return the idReserva
     */
    public int getIdReserva() {
        return idReserva;
    }

    /**
     * @param idReserva the idReserva to set
     */
    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    /**
     * @return int return the estat
     */
    public int getEstat() {
        return estat;
    }

    /**
     * @param estat the estat to set
     */
    public void setEstat(int estat) {
        this.estat = estat;
    }


    /**
     * @return Clients return the clients
     */
    public Clients getClients() {
        return clients;
    }

    /**
     * @param clients the clients to set
     */
    public void setClients(Clients clients) {
        this.clients = clients;
    }

    /**
     * @return Ofertes return the ofertes
     */
    public Ofertes getOfertes() {
        return ofertes;
    }

    /**
     * @param ofertes the ofertes to set
     */
    public void setOfertes(Ofertes ofertes) {
        this.ofertes = ofertes;
    }

}
