package shop.beautyservice.beautyservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import shop.beautyservice.beautyservice.domain.Usuaris;

public interface UsuariService {
    public List<Usuaris> llistarTotsElsUsuaris();
    public Optional llistarUsuarisPerId(int id);
    public int afexir(Usuaris u);
    public void esborrar(int id);
    
}
