package shop.beautyservice.beautyservice.repository;

import org.springframework.stereotype.Repository;

import shop.beautyservice.beautyservice.domain.Usuaris;

//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;


@Repository 
public interface UsuarisRepository extends CrudRepository<Usuaris,Integer >{
    
}
