package shop.beautyservice.beautyservice.controller;

import java.lang.ProcessBuilder.Redirect;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import shop.beautyservice.beautyservice.domain.Usuaris;
import shop.beautyservice.beautyservice.service.UsuariService;

@Controller
@RequestMapping
public class HomeController {

  @Autowired
  private UsuariService usuariService;

  @GetMapping("/llistarusuaris")
  public String index(Model modelo) {
     String dades="hola";
    
     List<Usuaris> usuaris=usuariService.llistarTotsElsUsuaris();
      modelo.addAttribute("usuaris",usuaris);
      return "index";
    }

    @GetMapping("/nou")
    public String nou(Model modelo) {      
        modelo.addAttribute("objectePersona",new Usuaris());
        return "formulari";
      }

      @PostMapping("/guardar")
      public String guardar(@Valid Usuaris u, Model modelo) { 
        usuariService.afexir(u);     
          return "redirect:/llistarusuaris";
        }

        @GetMapping("/editar/{id}")
        public String editar(@PathVariable int id, Model modelo) { 
          Optional<Usuaris> editarUsuari= usuariService.llistarUsuarisPerId(id);
          modelo.addAttribute("objectePersona",editarUsuari);          
            return "formulari";
          }

          @GetMapping("/esborrar/{id}")
          public String esborrar(@PathVariable int id, Model modelo) { 
             usuariService.esborrar(id);
             return "redirect:/llistarusuaris";
            }
  

}